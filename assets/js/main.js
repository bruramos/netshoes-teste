
/**
 * Representação da tela
 */
function model() {
    var self = this;
    self.posts = ko.observableArray("");
    self.postsCookie = ko.observableArray("");
    self.query = ko.observable("");
    self.productsList = ko.computed(function () {
        return self.posts();
    });
    self.bagList = ko.computed(function () {
        return self.postsCookie();
    });

    addItem = function (item) {
        addItemInCookie(item);
    }

    removeItem = function (item) {
        self.postsCookie([]);
        removeItemBag(item);
    }

    totalItems = function (){
        return self.postsCookie;
    }
}

/**
 * Cria objeto que representa a tela
 */
var mymodel = new model();

function loadProducts(data) {
    $.each(data, function () {
        $.each(this, function () {
            var self = this;
            mymodel.posts.push(new contentProduct(self));
        });
    });
}

/**
 * Itera o Array Observável da view a partir dos itens no Cookie
 */
function loadCookie(data) {
    mymodel.postsCookie.push(new contentProduct(data));
}

/**
 * Atualiza a lista do Cookie a cada alteração
 */
function setCookie(items) {
    $.cookie("items", JSON.stringify(items));
}

/**
 * Adiciona um novo Item no Cookie
 */
function addItemInCookie(item) {
    var cookieItems = getItemsFromCookie();
    cookieItems.push(item);
    setCookie(cookieItems);
    loadCookie(item);
}

/**
 * Busca os itens atuais armazenados no Cookie
 */
function getItemsFromCookie() {
    var items = $.cookie("items");
    if (items === undefined) {
        return new Array();
    }
    return JSON.parse(items);
}

/**
 * Trata o formato de exibição das parcelas de cada produto
 */
function installments(parcelas, valor){
    var valorFinal = valor;
    var parcela = valorFinal / parcelas;

    if(parcelas > 0){
        return "OU " + parcelas + "X R$ " + parcela.toFixed(2);
    } else{
        return "";
    }
}

/**
 * Representação do modelo do Produto
 */
function contentProduct(self) {
    this.productTitle = ko.observable(self.title);
    this.formatPrice = ko.observable(self.currencyFormat);
    this.productPrice = ko.observable(self.price + '0');
    this.productInstallments = ko.observable( installments(self.installments, self.price) );
    this.productStyle = ko.observable(self.style);
    this.id = ko.observable(self.id);
    this.item = ko.observable(self);
}


/**
 * Carrega os itens na Sacola a partir do Cookie
 */
function loadBag() {
    var items = getItemsFromCookie();
    $.each(items, function () {
        loadCookie(this);
    });
}

/**
 * Remove o item indicado da sacola
 */
function removeItemBag(item) {
    var items = getItemsFromCookie();
    for (var i = 0; i < items.length; i++) {
        if (items[i].id == item.id) {
            items.splice(i, 1);
            break;
        }
    }
    setCookie(items);
    mymodel.bagList = ko.observableArray("");
    loadBag();
}

$(document).ready(function () {
    /**
     * Lê os dados do JSON
     */
    $.getJSON("data/products.json", function (data) {
        loadProducts(data);
    });

    /**
     * Carrega os itens na Sacola a partir do Cookie
     */
    loadBag();

    /**
     * Faz o binding do modelo com a view
     */
    ko.applyBindings(mymodel);

    /**
     * Exibe a Sacola
     */
    $(".shopping-bag").click(function () {
        $(".section-bag").css("margin-right", "0");
    });

    /**
     * Fecha a Sacola
     */
    $(".section-bag-btn-close").click(function () {
        $(".section-bag").css("margin-right", "-60%");
    });

});



