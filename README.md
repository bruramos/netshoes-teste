## Instalação

Na pasta raiz do projeto:
1. Instale as dependências NPM: `npm install`
2. Instale as dependências BOWER: `bower install`
3. Inicie o servidor com o comando: `node app.js`
4. Acesse a aplicação no browser com o endereço: [ http://127.0.0.1:9090 ]


## Tecnologias

1. NodeJS
    - Servidor criado em NodeJS por definição da equipe
1. KnockoutJS
    - Escolhido pela simplicidade e fácil manipulação de observáveis, facilitando os bindings entre a view e o model
2. Jquery
    - Escolhido pela quantidade de funções simplificadas disponibilizadas pela lib
3. Bootstrap
    - Escolhido pela agilidade no desenvolvimento MobileFirst, sendo inportado módulos específicos para utilização apelas do necessário para o teste
4. Font-Awesome
    - Escolhido por ser umas das bibliotecas de icones mais completas
5. LESS
    - Foi o pré-processador escolhido para agilizar o desenvolvimento da folha de estilo